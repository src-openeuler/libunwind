%bcond_with tests

Name: libunwind
Epoch: 2
Version: 1.8.1
Release: 1
Summary: Libunwind provides a C ABI to determine the call-chain of a program
License: MIT
URL: https://www.nongnu.org/libunwind/
Source: https://github.com/libunwind/libunwind/releases/download/v%{version}/%{name}-%{version}.tar.gz

Patch9000: riscv.patch

ExclusiveArch: aarch64 %{ix86} x86_64 riscv64 ppc64le loongarch64

BuildRequires: gcc-c++
BuildRequires: /usr/bin/latex2man

%description
Libunwind defines a portable and efficient C programming interface (API) to
determine the call-chain of a program. The API additionally provides the means
to manipulate the preserved (callee-saved) state of each call-frame and to
resume execution at any point in the call-chain (non-local goto). The API
supports both local (same-process) and remote (across-process) operation.

%package devel
Summary: Development files for libunwind
Requires: libunwind = %{epoch}:%{version}-%{release}

%description devel
This package contains development files for libunwind.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%global optflags %{optflags} -fcommon

%configure --enable-static --enable-shared --enable-setjmp=no %{!?with_test:--disable-tests}
%make_build

%install
%make_install
%delete_la

# /usr/include/libunwind-ptrace.h
# [...] aren't really part of the libunwind API.  They are implemented in
# a archive library called libunwind-ptrace.a.
mv -f $RPM_BUILD_ROOT%{_libdir}/libunwind-ptrace.a $RPM_BUILD_ROOT%{_libdir}/libunwind-ptrace.a-save
rm -f $RPM_BUILD_ROOT%{_libdir}/libunwind*.a
mv -f $RPM_BUILD_ROOT%{_libdir}/libunwind-ptrace.a-save $RPM_BUILD_ROOT%{_libdir}/libunwind-ptrace.a
rm -f $RPM_BUILD_ROOT%{_libdir}/libunwind-ptrace*.so*

# fix multilib conflicts
touch -r NEWS $RPM_BUILD_ROOT%{_includedir}/libunwind.h

%if %{with tests}
# drop installed test programs
rm -fr %{buildroot}%{_libexecdir}

%check
%make_build check
%endif

%files
%license COPYING
%{_libdir}/libunwind*.so.*

%files devel
%{_libdir}/pkgconfig/libunwind*.pc
# <unwind.h> does not get installed for REMOTE_ONLY targets - check it.
%{_includedir}/unwind.h
%{_includedir}/libunwind*.h
%{_libdir}/libunwind-ptrace.a
%{_libdir}/libunwind*.so

%files help
%doc README NEWS
%{_mandir}/*/*

%changelog
* Sun Sep 29 2024 Funda Wang <fundawang@yeah.net> - 2:1.8.1-1
- update to 1.8.1
- disable test build by default, as it produces error under aarch64
  (https://github.com/libunwind/libunwind/issues/788)

* Thu Sep 12 2024 liweigang <liweiganga@uniontech.com> - 2:1.8.0-2
- Type:
- ID:NA
- SUG:NA
- DESC: del the source of the lower version

* Mon Aug  5 2024 dillon chen<dillon.chen@gmail.com> - 2:1.8.0-1
- Type:
- ID:NA
- SUG:NA
- DESC:update version to 1.8.0

* Thu Apr 11 2024 shafeipaozi <sunbo.oerv@isrc.iscas.ac.cn> - 2:1.7.2-3
- add support riscv64

* Mon Feb 26 2024 doupengda <doupengda@loongson.cn> - 2:1.7.2-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add support for loongarch64

* Mon Jan 29 2024 zhuofeng <zhuofeng2@huawei.com> - 2:1.7.2-1
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:update version to 1.7.2

* Mon Nov 27 2023 jiahua.yu <jiahua.yu@shingroup.cn> - 2:1.6.2-8
- Type:update
- ID:NA
- SUG:NA
- DESC:init support for ppc64le

* Tue Jul 11 2023 chenziyang <chenziyang4@huawei.com> - 2:1.6.2-7
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:backport upstream patch to fix failed run-ptrace-mapper testcase 

* Mon Jul 03 2023 chenziyang <chenziyang4@huawei.com> - 2:1.6.2-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix failed testcase by editing test-ptrace testcase

* Thu Jun 01 2023 wangjiang <wangjiang37@h-partners.com> - 2:1.6.2-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix failed testcase

* Tue Feb 21 2023 shixuantong <shixuantong1@huawei.com> - 2:1.6.2-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix some test fail

* Fri Dec 16 2022 Bin Hu <hubin73@huawei.com> - 2:1.6.2-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix byte_order_is_valid function logic

* Tue Apr 26 2022 renhongxun <renhongxun@h-partners.com>-2:1.6.2-2
- Type:update
- ID:NA
- SUG:NA
- DESC:add epoch

* Mon Jan 24 2022 mc964203886 <machi@iscas.ac.cn>-1.6.2-1
- Type:
- ID:NA
- SUG:NA
- DESC:update version 1.6.2

* Fri Nov 19 2021 shangyibin<shangyibin1@huawei.com> - 1.5.0-1
- Type:
- ID:NA
- SUG:NA
- DESC:update version 1.5.0

* Fri Jul 30 2021 liudabo<liudabo1@huawei.com> - 1.4.0-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Fix gcc build

* Mon Mar 30 2020 wenzhanli<wenzhanli2@huawei.com> - 1.4.0-1
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:update version 1.4.0

* Sun Jan 12 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.3.1-3
- remove useless patch

* Fri Sep 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.3.1-2
- del unnecessary statement

* Mon Sep 2 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.3.1-1
- Package init
